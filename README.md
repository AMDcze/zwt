# O repozitáři #

Tento repozitář slouží jako příprava na státní závěrečnou zkoušku (SZZ) z oboru Znalostní a webové technologie (ZWT) na Vysoké škole ekonomické v Praze (VŠE).

Je vytvořen nástrojem [XMind: ZEN](https://www.xmind.net/zen/) ve formě myšlenkových map (mind maps). 

### 1) Předmět SZZ Znalostní technologie (4IZ410 + 4IZ430 + 4IZ450 + 4IZ460) ###
* a) Entropie a kódování
    * Shannonovská míra informace a entropie
    * vlastnosti kódů
    * konstrukce optimálních kódů
    * samoopravné kódy
    * přenos zpráv komunikačním systémem se šumem
* b) Konstrukce rozhodovacích stromů
    * rozhodovací tabulky a jejich vlastnosti
    * vztah střední délky a entropie
    * optimální rozhodovací strom
    * různé algoritmy konstrukce rozhodovacích stromů
* c) Výroková, predikátová a deskripční logika
    * základní pojmy
    * pravdivost formulí
    * axiomy
    * odvozovací pravidla
    * metody odvozování
    * dokazatelnost
    * rezoluční princip
    * automatické dokazování
* d) Fuzzy množiny a fuzzy logika
    * fuzzy množiny
    * fuzzy relace
    * fuzzy regulace
    * fuzzy logické spojky
    * axiomatizace fuzzy výrokové logiky
    * Lukasiewiczova logika
    * součinová logika
    * Gödelova logika
* e) Bayesovské sítě
    * reprezentace podmíněných nezávislostí
    * inference v Bayesovských sítích
    * pseudobayesovská inference v expertních systémech
* f) Řešení úloh a rozhodovací problém
    * stavový prostor a jeho prohledávání
    * rozhodování za rizika a neurčitosti
* g) Strojové učení a adaptace
    * základní principy a metody
    * učení a adaptace
    * teoretické problémy strojového učení
* h) Multiagentní systémy
    * reaktivní agenty
    * deliberartivní agenty
    * interakce mezi agenty
* i) Proces a úlohy dobývání znalostí
    * definice dobývání znalostí
    * metodika CRISP-DM a další standardy
    * úlohy klasifikace
    * predikce
    * segmentace
    * hledání asociací
* j) Zdroje dobývání znalostí
    * databáze
    * statistické metody
    * strojové učení
* k) Metody dobývání znalostí
    * rozhodovací stromy
    * asociační pravidla
    * rozhodovací pravidla
    * neuronové sítě
    * genetické algoritmy
    * bayesovské metody
    * učení založené na instancích
    * kombinování klasifikačních modelů
* l) Způsoby hodnocení modelů získaných metodami dobývání znalostí
    * matice záměn
    * správnost a chyba učení
    * přesnost a úplnost
    * sensitivita a specificita
    * ROC křivky
* m) Metody předzpracování dat pro algoritmy dobývání znalostí
    * sampling
    * selekce atributů
    * transformace atributů
    * diskretizace
    * seskupování hodnot atributů
    * ošetření chybějících hodnot
* n) Metoda GUHA a GUHA procedury
    * procedura ASSOC
    * GUHA procedury pro práci s dvojicemi asociačních pravidel
    * GUHA procedury pro práci s histogramy
    * GUHA procedury pro práci s dvojicemi kategoriálních atributů
    * procedura MCluster-Miner
* o) Observační kalkuly
    * třídy 4ft-kvantifikátorů
    * dedukční pravidla
    * práce s neúplnou informací

### 2) Předmět SZZ Webové technologie (4IZ421 + 4IZ440 + 4IZ470) ###
* a) Zásady tvorby a zpracování informačních fondů
    * podstata informačního procesu
    * typologie dokumentů
    * informační analýza – identifikační
    * obsahová
    * indexování
    * selekční jazyky a jejich význam pro vyhledávání
    * vlivy globalizace a automatizace
* b) Metody vyhledávání v automatizovaných informačních fondech
    * booleovský model
    * vektorový model
    * metody měření podobnosti
* c) Klasifikování a shlukování informačních fondů
    * podstata procesů klasifikování a shlukování
    * podobnosti a rozdíly
    * metody automatizace
    * využitelnost výsledků
* d) Základní problémy počítačového zpracování přirozeného jazyka a jejich vztah k automatizaci informačních systémů
    * potřeba slovníků
    * implementace morfologické, syntaktické a sémantické analýzy
    * zpracování homonymie
* e) Automatizace tvorby a využívání tezaurů v informačních systémech.
* f) Jazyk a datový model RDF, serializace RDF.
* g) Dotazovací jazyk SPARQL a jeho vyjadřovací možnosti.
* h) Struktura a způsob využívání slovníků pro propojená data (linked data), možnosti odvozování v jazyce RDFS.
* i) Principy linked data a způsoby vystavování dat v RDF na webu.
* j) Využívání propojených dat na webu v různých věcných oblastech; sklízení sémantických dat z webu (WebDataCommons)
    * encyklopedická data
    * data veřejné správy
    * elektronické obchodování
    * podniková integrace
    * vyhledávače
* k) Tvorba propojených dat (linked data) ze strukturovaných i nestrukturovaných dat, proces ETL pro linked data (extrakce, linkování atd.), tvorba aplikací nad propojenými daty.
* l) Předzpracování textových informací prostředky analýzy přirozeného jazyka
    * tokenizace
    * POS
    * koreference
    * syntaktická analýza
    * tezaurové transformace
* m) Ruční tvorba extrakčních modelů – regulární výrazy a jazyk JAPE. Rozpoznávání a linkování pojmenovaných entit.
* n) Extrakce strukturovaných záznamů z textu: wrapperový (založený na DOM resp. rendering) a statistický (HMM, CRF) přístup. Evaluace extrakce informací.
* o) Extrakce relací z webu – doménově specifická (vzory Hearstové, DIPRE), otevřená extrakce (OpenIE, Watson).
* p) Dolování ze struktury webu (WSM): algoritmy PageRank a HITS. Využití personalizace při WSM. Globální webgraf.
* q) Dolování z uživatelského přístupu k webu (WUM). Analýza clickstreamů. Webová analytika v praxi.